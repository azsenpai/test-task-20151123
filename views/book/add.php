<?php

use yii\helpers\Url;
use yii\web\View;

$this->title = 'Add a new book';

$this->params['breadcrumbs'][] = ['label' => 'Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile('lib/bootstrap-datepicker/bootstrap-datepicker.min.css', ['depends' => 'yii\bootstrap\BootstrapAsset']);
$this->registerJsFile('lib/bootstrap-datepicker/bootstrap-datepicker.min.js', ['depends' => 'yii\web\JqueryAsset'], 'bootstrap-datepicker-js');

$this->registerJsFile('js/book-add.js', ['depends' => 'bootstrap-datepicker-js']);

?>
<h1 class="title">Add a new book</h1>

<?php if ($message = Yii::$app->session->getFlash('book-add-success')): ?>
    <p class="alert alert-success"><?= $message; ?></p>
<?php endif; ?>

<?php if ($message = Yii::$app->session->getFlash('book-add-error')): ?>
    <div class="alert alert-warning"><?= $message; ?></div>
<?php endif; ?>

<form action="<?= Url::to(['book/add']); ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>">
    <div class="form-group">
        <label for="edit-name">Name</label>
        <input class="form-control" name="Book[name]" id="edit-name">
    </div>
    <div class="form-group">
        <label for="edit-preview">Preview</label>
        <input type="file" name="Book[preview]" id="edit-preview">
    </div>
    <div class="form-group">
        <label for="edit-preview">Date of publication</label>
        <input class="form-control" name="Book[date]" id="edit-date">
    </div>
    <div class="form-group">
        <label for="edit-preview">Author</label>
        <select name="Book[author_id]" class="form-control">
            <option value="0">- Select author -</option>
            <?php for ($i = 0, $n = count($authors); $i < $n; $i ++): ?>
                <option value="<?= $authors[$i]->id; ?>"><?= $authors[$i]->firstname . ' ' . $authors[$i]->lastname; ?></option>
            <?php endfor; ?>
        </select>
    </div>
    <div class="text-center">
        <input type="submit" class="btn btn-success" value="Submit">
        <a href="<?= Url::to(['book/index']); ?>" class="btn btn-warning">Cancel</a>
    </div>
</form>
