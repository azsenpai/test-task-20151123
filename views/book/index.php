<?php

use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Book;

$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile('lib/bootstrap-datepicker/bootstrap-datepicker.min.css', ['depends' => 'yii\bootstrap\BootstrapAsset']);
$this->registerJsFile('lib/bootstrap-datepicker/bootstrap-datepicker.min.js', ['depends' => 'yii\web\JqueryAsset'], 'bootstrap-datepicker-js');

$this->registerCssFile('lib/fancybox/jquery.fancybox.css', ['depends' => 'app\assets\AppAsset']);
$this->registerJsFile('lib/fancybox/jquery.fancybox.js', ['depends' => 'yii\web\JqueryAsset'], 'jquery-fancybox-js');

$this->registerJsFile('js/book-index.js', ['depends' => 'jquery-fancybox-js'], 'book-index-js');

$queryParams = Yii::$app->request->getQueryParams();
$queryParams[0] = 'book/index';

$sortParams = [
    'id' => ['sort_by' => 'id', 'sort' => 'asc'],
    'name' => ['sort_by' => 'name', 'sort' => 'asc'],
    'author' => ['sort_by' => 'author', 'sort' => 'asc'],
    'date' => ['sort_by' => 'date', 'sort' => 'asc'],
    'date_create' => ['sort_by' => 'date_create', 'sort' => 'asc'],
];

if (isset($queryParams['sort_by']) && isset($queryParams['sort']) && isset($sortParams[$queryParams['sort_by']])) {
    if (empty($queryParams['sort'])) {
        $sortParams[$queryParams['sort_by']]['sort'] = 'asc';
    } else if ($queryParams['sort'] == 'asc') {
        $sortParams[$queryParams['sort_by']]['sort'] = 'desc';
    } else {
        $sortParams[$queryParams['sort_by']]['sort'] = '';
    }
}

?>
<h1 class="title"><?= $this->title; ?></h1>

<?php if (!\Yii::$app->user->isGuest): ?>
<div class="form-group">
    <a href="<?= Url::to(['book/add']); ?>" class="btn btn-success">Add a new book</a>
</div>
<?php endif; ?>

<style>
    .preview {
        width: 150px;
        height: auto;
    }
    .action-link {
        display: inline-block;
        margin: 0 0 0 5px;
    }
    .action-remove.disabled {
        cursor: default;
        color: #ccc;
    }
    .tl-f {
        table-layout: fixed;
    }
    .mb-0 {
        margin-bottom: 0;
    }
    .table.bt-0 > tbody > tr > td {
        border-top: 0;
    }
    .btn-submit {
        width: 80px;
    }
</style>

<?php if (!\Yii::$app->user->isGuest): ?>
<form action="<?= Url::to(['book/index']); ?>">
    <table class="table tl-f bt-0">
        <tbody>
            <tr>
                <td>
                    <select class="form-control" name="author_id">
                        <option value="0">- Any author -</option>
                        <?php for ($i = 0, $n = count($authors); $i < $n; $i ++): ?>
                            <option <?php if ($authors[$i]->id == $author_id) print 'selected'; ?> value="<?= $authors[$i]->id; ?>"><?= $authors[$i]->firstname . ' ' . $authors[$i]->lastname; ?></option>
                        <?php endfor; ?>
                    </select>
                </td>
                <td>
                    <input name="name" class="form-control" placeholder="Book name" value="<?= $name; ?>">
                </td>
            </tr>
            <tr>
                <td class="form-inline">
                    Date of publication:
                    <input name="date_from" class="form-control" value="<?= $date_from; ?>">
                    to
                    <input name="date_to" class="form-control" value="<?= $date_to; ?>">
                </td>
                <td>
                    <input type="submit" value="Find" class="btn btn-primary btn-submit">
                </td>
            </tr>
        </tbody>
    </table>
</form>
<?php endif; ?>

<hr>

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th><a class="<?php print Book::getSortClass($sortParams['id']['sort']); ?>" href="<?= Url::to(ArrayHelper::merge($queryParams, $sortParams['id'])); ?>">ID</a></th>
            <th><a class="<?php print Book::getSortClass($sortParams['name']['sort']); ?>" href="<?= Url::to(ArrayHelper::merge($queryParams, $sortParams['name'])); ?>">Name</a></th>
            <th>Preview</th>
            <th><a class="<?php print Book::getSortClass($sortParams['author']['sort']); ?>" href="<?= Url::to(ArrayHelper::merge($queryParams, $sortParams['author'])); ?>">Author</a></th>
            <th><a class="<?php print Book::getSortClass($sortParams['date']['sort']); ?>" href="<?= Url::to(ArrayHelper::merge($queryParams, $sortParams['date'])); ?>">Date of publication</a></th>
            <th><a class="<?php print Book::getSortClass($sortParams['date_create']['sort']); ?>" href="<?= Url::to(ArrayHelper::merge($queryParams, $sortParams['date_create'])); ?>">Date create</a></th>
            <?php if (!\Yii::$app->user->isGuest): ?>
            <th>Actions</th>
            <?php endif; ?>
        </tr>
    </thead>
    <tbody>
        <?php for ($i = 0, $n = count($books); $i < $n; $i ++): $book = $books[$i]; ?>
        <tr data-book_id="<?= $book['id']; ?>">
            <td class="id"><?= $book['id']; ?></td>
            <td class="name"><?= $book['name']; ?></td>
            <td class="preview">
                <a href="/uploads/<?= $book['preview']; ?>" class="fancybox">
                    <img alt="" src="/uploads/<?= $book['preview']; ?>" class="preview">
                </a>
            </td>
            <td class="author"><?= $book['author']; ?></td>
            <td class="dop"><?= date('j F Y', strtotime($book['date'])); ?></td>
            <td class="date">
                <?php
                    $timestamp = strtotime($book['date_create']);

                    if (date('Y-m-d') == date('Y-m-d', $timestamp)) {
                        print 'Today, ' . date('H:i:s', $timestamp);
                    } else if (date('Y-m-d', time() - 24*60*60) == date('Y-m-d', $timestamp)) {
                        print 'Yesterday, ' . date('H:i:s', $timestamp);
                    } else {
                        print date('j F Y, H:i:s', $timestamp);
                    }
                ?>
            </td>
            <?php if (!\Yii::$app->user->isGuest): ?>
            <td>
                <a class="action-link action-update" href="/book/update/<?= $book['id']; ?>" title="Update" aria-label="Update"><span class="glyphicon glyphicon-pencil"></span></a>
                <a class="action-link action-view" href="#book" title="View" aria-label="View"><span class="glyphicon glyphicon-eye-open"></span></a>
                <a class="action-link action-remove" href="#" title="Delete" aria-label="Delete"><span class="glyphicon glyphicon-trash"></span></a>
            </td>
            <?php endif; ?>
        </tr>
        <?php endfor; ?>
    </tbody>
</table>

<div class="text-center">
    <?= \yii\widgets\LinkPager::widget(['pagination' => $pages]); ?>
</div>

<div id="book" style="display: none">
    <table class="table table-striped table-bordered tl-f mb-0">
        <tr>
            <td>ID</td>
            <td class="id"></td>
        </tr>
        <tr>
            <td>Name</td>
            <td class="name"></td>
        </tr>
        <tr>
            <td>Preview</td>
            <td class="preview"></td>
        </tr>
        <tr>
            <td>Author</td>
            <td class="author"></td>
        </tr>
        <tr>
            <td>Date of publication</td>
            <td class="dop"></td>
        </tr>
        <tr>
            <td>Date create</td>
            <td class="date"></td>
        </tr>
    </table>
</div>
