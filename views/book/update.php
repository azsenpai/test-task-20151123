<?php

use yii\helpers\Url;
use yii\web\View;

$this->title = 'Update';

$this->params['breadcrumbs'][] = ['label' => 'Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = $book_id;

$this->registerCssFile('lib/bootstrap-datepicker/bootstrap-datepicker.min.css', ['depends' => 'yii\bootstrap\BootstrapAsset']);
$this->registerJsFile('lib/bootstrap-datepicker/bootstrap-datepicker.min.js', ['depends' => 'yii\web\JqueryAsset'], 'bootstrap-datepicker-js');

$this->registerJsFile('js/book-update.js', ['depends' => 'bootstrap-datepicker-js']);

?>
<h1 class="title">Update</h1>

<?php if ($message = Yii::$app->session->getFlash('book-update-error')): ?>
    <div class="alert alert-warning"><?= $message; ?></div>
<?php endif; ?>

<form action="<?= Url::to(['book/update/' . $book_id]); ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>">
    <div class="form-group">
        <label for="edit-name">Name</label>
        <input class="form-control" name="Book[name]" id="edit-name" value="<?= $model->name; ?>">
    </div>
    <div class="form-group">
        <label for="edit-preview">Preview</label>
        <input type="file" name="Book[preview]" id="edit-preview">
    </div>
    <div class="form-group">
        <label for="edit-preview">Date of publication</label>
        <input class="form-control" name="Book[date]" id="edit-date" value="<?php if (!empty($model->date)) print date('d/m/Y', strtotime($model->date)); ?>">
    </div>
    <div class="form-group">
        <label for="edit-preview">Author</label>
        <select name="Book[author_id]" class="form-control">
            <option value="0">- Select author -</option>
            <?php for ($i = 0, $n = count($authors); $i < $n; $i ++): ?>
                <option <?php if ($model->author_id == $authors[$i]->id) print 'selected'; ?> value="<?= $authors[$i]->id; ?>"><?= $authors[$i]->firstname . ' ' . $authors[$i]->lastname; ?></option>
            <?php endfor; ?>
        </select>
    </div>
    <div class="text-center">
        <input type="submit" class="btn btn-success" value="Update">
        <a href="<?= Url::to(['book/index']); ?>" class="btn btn-warning">Cancel</a>
    </div>
</form>
