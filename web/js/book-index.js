jQuery(function($) {
    $('.action-remove').on('click', function(e) {
        e.preventDefault();

        var $this = $(this);

        if ($this.hasClass('disabled')) {
            return;
        }

        var answer = confirm('Really want to remove this book');

        if (!answer) return;

        var $tr = $this.parents('tr');

        $this.addClass('disabled');

        $.ajax({
            url: '/book/remove/' + $tr.data('book_id'),
            dataType: 'json',
            success: function(response) {
                if (('status' in response) && response.status == 'ok') {
                    $tr.remove();
                }
            },
            complete: function() {
                $this.removeClass('disabled');
            }
        });
    });

    $('.fancybox').fancybox({
        helpers: {
            overlay: { locked: false }
        }
    });

    $('.action-view').on('click', function(e) {
        e.preventDefault();

        var $tr = $(this).parents('tr');
        var $book = $('#book');

        $book.find('.id').html($tr.find('.id').html());
        $book.find('.name').html($tr.find('.name').html());
        $book.find('.preview').html($tr.find('.preview').html());
        $book.find('.author').html($tr.find('.author').html());
        $book.find('.dop').html($tr.find('.dop').html());
        $book.find('.date').html($tr.find('.date').html());
    });

    $('.action-view').fancybox({
        padding: 23,
        width: '60%',
        height: 'auto',
        autoSize: false,
        helpers: {
            overlay: { locked: false }
        }
    });

    $('[name="date_from"]').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
    });

    $('[name="date_to"]').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
    });
});
