jQuery(function($) {
    $('[name="Book[date]"]').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
    });
});
