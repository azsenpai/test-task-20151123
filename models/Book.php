<?php

namespace app\models;

use app\models\Author;

class Book extends \yii\db\ActiveRecord
{
    public static function getSortClass($sort)
    {
        if (empty($sort)) {
            return 'desc';
        } else if ($sort == 'desc') {
            return 'asc';
        }

        return '';
    }

    public static function tableName()
    {
        return 'books';
    }

    public function rules()
    {
        return [
            [['name', 'date', 'author_id'], 'required'],
        ];
    }
}
