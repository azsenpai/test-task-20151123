<?php

namespace app\models;

class Author extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'authors';
    }
}
