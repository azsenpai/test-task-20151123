<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\Response;
use app\models\Book;
use app\models\Author;

class BookController extends Controller
{
    public function actionIndex()
    {
        $query = Book::find()
            ->select(['books.*', 'CONCAT(firstname, " ", lastname) AS author'])
            ->leftJoin('authors', 'authors.id = books.author_id')
        ;

        $author_id = (int)Yii::$app->request->get('author_id', 0);

        if ($author_id > 0) {
            $query->andWhere('author_id = :author_id', [':author_id' => $author_id]);
        }

        $name = Yii::$app->request->get('name', '');

        if (!empty($name)) {
            $query->andWhere('name LIKE :name', [':name' => '%' . $name . '%']);
        }

        $date_from = Yii::$app->request->get('date_from', '');

        if (preg_match('#^\d{2}/\d{2}/\d{4}$#', $date_from)) {
            list($d, $m, $y) = explode('/', $date_from);
            $query->andWhere('date >= :date_from', [':date_from' => $y . '-' . $m . '-' . $d]);
        } else {
            $date_from = '';
        }

        $date_to = Yii::$app->request->get('date_to', '');

        if (preg_match('#^\d{2}/\d{2}/\d{4}$#', $date_to)) {
            list($d, $m, $y) = explode('/', $date_to);
            $query->andWhere('date <= :date_to', [':date_to' => $y . '-' . $m . '-' . $d]);
        } else {
            $date_to = '';
        }

        $sort_by = Yii::$app->request->get('sort_by');
        $sort = Yii::$app->request->get('sort');

        if (!empty($sort_by) && !empty($sort)) {
            if ($sort_by == 'id') {
                $sort_by = 'books.id';
            } else if ($sort_by == 'author') {
                $sort_by = 'firstname, lastname';
                if ($sort == 'desc') {
                    $sort_by = 'firstname DESC, lastname';
                }
            }

            if ($sort == 'asc') {
                $query->orderBy($sort_by);
            } else if ($sort == 'desc') {
                $query->orderBy($sort_by . ' DESC');
            }
        }

        $limit = Yii::$app->params['per_page'];
        $page = (int)Yii::$app->request->get('page', 1) - 1;

        $total = (int)$query->count();

        $books = $query->offset($page * $limit)->limit($limit)->asArray()->all();

        $pages = new \yii\data\Pagination(['totalCount' => $total, 'pageSize' => $limit]);

        $authors = Author::find()->all();

        return $this->render('index', [
            'books' => $books,
            'pages' => $pages,
            'authors' => $authors,
            'author_id' => $author_id,
            'name' => $name,
            'date_from' => $date_from,
            'date_to' => $date_to,
        ]);
    }

    public function actionAdd()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['index']);
        }

        $model = new Book();
        $authors = Author::find()->all();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if ($model->validate()) {
                if (preg_match('#^\d{2}/\d{2}/\d{4}$#', $model->date)) {
                    list($d, $m, $y) = explode('/', $model->date);
                    $model->date = $y . '-' . $m . '-' . $d;
                } else {
                    $model->date = date('Y-m-d');
                }

                $preview = UploadedFile::getInstance($model, 'preview');

                if (is_null($preview) || !in_array($preview->extension, ['jpeg', 'jpg', 'png'])) {
                    $file = 'default.jpg';
                } else {
                    $file = md5($preview->baseName . uniqid()) . '.' . $preview->extension;
                    $preview->saveAs('uploads/' . $file);
                }

                $model->preview = $file;

                if ($model->save()) {
                    Yii::$app->session->setFlash('book-add-success', '<b>'. $model->name . '</b>' . ' added successfully');
                    return $this->redirect(['add'], ['authors' => $authors]);
                }
            }

            $message  = '<ol style="margin: 0">';

            foreach ($model->errors as $error) {
                $message .= '<li>';
                $message .= implode(', ', $error);
                $message .= '</li>';
            }

            $message .= '</ol>';

            Yii::$app->session->setFlash('book-add-error', $message);
        }

        return $this->render('add', ['authors' => $authors]);
    }

    public function actionUpdate($id = 0)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['index']);
        }

        $model = Book::findOne($id);
        $authors = Author::find()->all();

        if (is_null($model)) {
            return $this->redirect(['index']);
        }

        if (strpos(Yii::$app->request->referrer, '/book/index') !== FALSE) {
            Yii::$app->session->setFlash('book-update-' . $id, Yii::$app->request->referrer);
        }

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if ($model->validate()) {
                if (preg_match('#^\d{2}/\d{2}/\d{4}$#', $model->date)) {
                    list($d, $m, $y) = explode('/', $model->date);
                    $model->date = $y . '-' . $m . '-' . $d;
                } else {
                    $model->date = date('Y-m-d');
                }

                $preview = UploadedFile::getInstance($model, 'preview');

                if (!is_null($preview) && in_array($preview->extension, ['jpeg', 'jpg', 'png'])) {
                    $file = md5($preview->baseName . uniqid()) . '.' . $preview->extension;
                    $preview->saveAs('uploads/' . $file);
   
                    $model->preview = $file;
                }

                if ($model->save()) {
                    $url = Yii::$app->session->getFlash('book-update-' . $id);

                    if (!empty($url)) {
                        return $this->redirect($url);
                    }

                    return $this->redirect(['index']);
                }
            }

            $message  = '<ol style="margin: 0">';

            foreach ($model->errors as $error) {
                $message .= '<li>';
                $message .= implode(', ', $error);
                $message .= '</li>';
            }

            $message .= '</ol>';

            Yii::$app->session->setFlash('book-update-error', $message);
        }

        return $this->render('update', [
            'model' => $model,
            'authors' => $authors,
            'book_id' => $id,
        ]);
    }

    public function actionRemove($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $result = [];

        if (Yii::$app->user->isGuest) {
            $result['status'] = 'error';
        } else {
            if (Book::deleteAll('id = :id', [':id' => $id])) {
                $result['status'] = 'ok';
            } else {
                $result['status'] = 'error';
            }
        }

        return $result;
    }
}
